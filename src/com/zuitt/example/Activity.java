package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        //Array
        String[] fruits = {"apple", "avocado", "banana", "kiwi", "orange"};
        System.out.println("Fruits in stock: " + Arrays.toString(fruits));
        System.out.println("Which fruit would you like to get the index of?");
        String pickedFruit = scan.nextLine();

        int index = -1;
        for(int a = 0; a < fruits.length; a++){
            if(fruits[a].equals(pickedFruit)){
                index = a;
            }
        }
       if(index == -1){
            System.out.println("Fruit is not in the array");
        }else{
            System.out.println("The index of " + pickedFruit + " is: " + index);
        }

        System.out.println();
        //Array List
        ArrayList<String> friends = new ArrayList<>(Arrays.asList("Clark", "Maverick", "Bryan", "Hubert"));
        System.out.println("My friends are: " + friends);


        System.out.println();
        //HashMap
        HashMap<String, Integer> products = new HashMap<>();
        products.put("toothpaste", 15);
        products.put("toothbrush", 20);
        products.put("soap", 12);
        System.out.println("Our current inventory consists of:");
        System.out.println(products);
    }
}
